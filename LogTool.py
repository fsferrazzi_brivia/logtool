########################
##### SETUP
########################

print("Importando bibliotecas...")
#Importa bibliotecas
import re
import pandas as pd
import datetime
import numpy as np
import os

print("Carregando funções...")
#Funcao que converte formato de hora para numero total de segundos
def dhms_to_sec(dhms_str):
    '''supports also format of d:h:m:s, h:m:s, m:s and s'''
    _,d,h,m,s = (':0'*10+dhms_str).rsplit(':',4)
    return int(d)*24*60*60+int(h)*60*60+int(m)*60+int(s) 

#Funcao que converte numero de minutos para formato de hora e minuto
def min_to_hm(min_str):
    m = int(min_str)
    d = m//60//24
    if d>0: 
        m = m % (d*60*24)
    h = m//60
    if h>0: 
        m = m % (h*60)
    return( str(h).zfill(2) +':'+ str(m).zfill(2))

# Versao vetorizada da funcao de conversao de formato de minutos
vec_m2hm = np.vectorize(min_to_hm)

########################
##### INICIO DO WORKFLOW
########################


### IMPORTA O ARQUIVO DE LOGS
print("Digite o local do arquivo de logs:")
userEntry = input()  
print("Importando logs do dia...")
try:
    file1 = open(userEntry,"r") 
    texto = file1.read()
    file1.close()
except IOError:
    print("Nao foi possivel abrir o arquivo.")
    os.system("pause")
    raise SystemExit
    #err - arquivo nao encontrado
    #err - arquivo em formato invalido

print("DEBUG")
print(texto[1:100])
os.system("pause")


### IDENTIFICANDO LOGS
print("Identificando logs...")
#Localiza a tag #ENDLOG
splitpnt = texto.find('#ENDLOG')
print(".")
os.system("pause")
#Corta o restante do texto fora
texto2 = texto[0:splitpnt]
print(".")
os.system("pause")
#Encontra os timestamps
texto3 = list(re.finditer("\d\d:\d\d\s\d\d/\d\d/\d\d\d\d",texto2))
print(".")
os.system("pause")
nLogs = len(texto3)
print( str(nLogs) + " logs encontrados.")
#Checando se tem pelo menos dois logs
if nLogs < 3:
    print("Número muito pequeno de logs. Terminando operação.")
    os.system("pause")
    raise SystemExit


#Quebra os logs em uma lista
a = 0
logs=[0]*nLogs
for i in range(nLogs-1):
    logs[i] = texto2[texto3[i].span()[0]:texto3[i+1].span()[0]]
    i




#Ultimo log calculado separadamente para pegar ate o fim do texto
logs[nLogs-1] = texto2[texto3[nLogs-1].span()[0]:len(texto2)]
#Coloca os logs no formato do data frame
df_dia = pd.DataFrame(columns=["Hora","Data","Projeto","Notas","Total min."])




i = 0
for i in range(nLogs):
    #coloca o log no formato do data frame
    aux = pd.DataFrame({"Hora":[logs[i][0:5]],
                        "Data":[logs[i][7:16]],
                        "Projeto":[logs[i][17:logs[i].find('\n')]],
                        "Notas":[logs[i][(logs[i].find('\n')+1):len(logs[i])-1]],
                        "Total min.": 0 })
    df_dia = df_dia.append(aux, sort = True)
    



#calcula o tempo transcorrido para cada log
i=0
for i in range(nLogs-1):
    start_time = dhms_to_sec(df_dia.at[0,"Hora"][i]+":00")
    stop_time  = dhms_to_sec(df_dia.at[0,"Hora"][i+1]+":00")
    df_dia.at[0,"Total min."][i] = (stop_time - start_time)//60
#err - tempo transcorrido negativo significa que logs estao fora de ordem
algum_total_negativo = any(elem < 0 for elem in df_dia.at[0,"Total min."])
if algum_total_negativo:
    print("Encontrado log com tempo total negativo. Conferir ordem dos timestamps!")
    print("Terminando operação.")
    os.system("pause")
    raise SystemExit

print("BEEP")
os.system("pause")

### TRATAMENTO DOS NOMES DE PROJETOS
#sobrescreve o nome do projeto do ultimo timestamp(ENCERRAMENTO)
df_dia.at[0,"Projeto"][-1] = "ENCERRAMENTO"

print(".")
os.system("pause")

#adapta nomes de projetos pegando a partir do primeiro alfanumerico e converte para maiusculas   
i=0
for i in range(nLogs-1):
    start_index = re.search("[A-Za-z]|[0-9]", df_dia.at[0,"Projeto"][i])
    if start_index is not None:
        df_dia.at[0,"Projeto"][i] = df_dia.at[0,"Projeto"][i][start_index.span()[0]:None].upper()
    
print("BEEP")
os.system("pause")


### CALCULA O TEMPO GASTO EM CADA PROJETO
print("Preparando report...")
df_report = pd.DataFrame(columns=["Data","Projeto","Total min."])
print(".")
os.system("pause")

#soma minutos de cada projeto
df_report['Total min.'] = df_dia.groupby('Projeto').agg({'Total min.':'sum'})['Total min.']
print(".")
os.system("pause")
#coloca indices de projeto para dentro do dataframe
df_report = df_report.drop('Projeto',axis=1)
df_report = df_report.reset_index()
df_report = df_report[['Data','Projeto','Total min.']]
print(".")
os.system("pause")
#remove projetos que tem zero minutos
df_report = df_report[df_report['Total min.'] != 0]
print(".")
os.system("pause")
#converte os minutos para o formato hh:mm
df_report['Total min.'] = vec_m2hm(df_report['Total min.'])
print(".")
os.system("pause")
#insere como data a primeira data do log 
df_report = df_report.assign(Data = df_dia.at[0,'Data'][0])
print(".")
os.system("pause")
#exporta em arquivo xlsx
file3 = 'LogReport.xlsx'
df_report = df_report.to_excel(file3,index=False)
print(".")
os.system("pause")
#err - problema ao exportar arquivo de reportes
#out - indicar que reporte foi gerado

print("BEEP")
os.system("pause")


### INSERE LOGS NOVOS NO HISTORICO
print("Atualizando historico...")
#Le arquivo com logs guardados e poe em um data frame
file2 = 'Logs.xlsx'
xl = pd.ExcelFile(file2)
snames = xl.sheet_names
df1 = xl.parse(snames[0])
print(".")
os.system("pause")
#err - problema ao importar arquivo de historico
#
#Remove colunas indesejadas
df1 = df1[aux.columns]
print(".")
os.system("pause")
#Insere linhas do dia no historico de logs
df1 = df1.append(df_dia, sort = True)
print(".")
os.system("pause")
#Sobrescreve arquivo de logs
df1.to_excel('Logs.xlsx',index=False)
print(".")
os.system("pause")
#err - problema ao exportar arquivo de historico
#out - indicar que historico foi atualizado

print("FIM")
os.system("pause")
